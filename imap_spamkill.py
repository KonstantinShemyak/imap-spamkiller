import ssl
import getpass
from imapclient import IMAPClient
from email.utils import parseaddr

host = "mail.example.com"
username = "user@example.com"
spam_folder = "Inbox.Spam"

ssl_context = ssl.create_default_context()

with IMAPClient(host=host, ssl_context=ssl_context) as client:
    password = getpass.getpass(prompt="Password for {} on {}: ".format(username, host))

    client.login(username, password)
    client.select_folder(spam_folder)

    messages = client.search(['NOT', 'DELETED'])
    print("%d messages in %s" % (len(messages), spam_folder))

    response = client.fetch(messages, ['BODY[HEADER.FIELDS (FROM)]'])

    msg_by_sender_name = {}
    for message_id, data in response.items():
        sender = "(no From field)"
        try:
            sender = data[b'BODY[HEADER.FIELDS (FROM)]']
        except KeyError:
            continue            # Do not care about those without From:

        sender_name, sender_address = parseaddr(str(sender))
        try:
            msg_by_sender_name[sender_name].append(message_id)
        except KeyError:
            msg_by_sender_name[sender_name] = [message_id]

    deleted_messages = 0
    for sender_name, ids in msg_by_sender_name.items():
        if len(ids) > 2:
            print("Deleting {} messages from \"{}\"".format(len(ids), sender_name))
            client.delete_messages(ids)
            deleted_messages += len(ids)

    print("Deleted {} messages".format(deleted_messages))
