# imap-spamkiller

## Simple actions in Python to delete some of spam from IMAP4 server.

Spammers frequently do not send just one spam - they send many.
I realized that a legitimate sender which I have not whitelisted,
i.e. with whom we did not have previous correspondence, will not
typically send more than two messages in one week.

This script finds all senders (as self-identified in the **"From"**
field) such that the IMAP4 folder contains more than two their 
messages. Then it **deletes** all mails from such senders.

Substitute your values for the server, folder, username and password.
Keep in mind that **your mails are being deleted**.
